# THX Présence Solidaire

Ceci est le dépôt `git` des sources du livre produit en 2021 par _petites singularités_.

Voir <https://thx.zoethical.org/pub/presence-solidaire>

## Usage

La version **pour écran** se trouve dans la branche `screen`. (v1.1.1)

La version **pour impression** se trouve dans la branche `print`. (v1.1.0)

## Modification des textes

Il est important de modifier les textes de manière consistante. 
La méthode utilisée est de modifier d'abord la version imprimée, puis reporter les changements dans la version pour écran.

Ne surtout pas écraser les images !

## Acheter le livre

Ce livre est distribué sous [licence Art Libre] et disponible gratuitement au lien indiqué ci-dessus.
Vous pouvez également vous procurer une version imprimée du livre au format A6 auprès des _petites singularités_.
Pour cela vous pouvez nous écrire par courriel ou répondre au sujet sur le forum et nous envoyer votre adresse postale afin que nous puissions vous indiquer les possibilités d'acheminement.
Nous acceptons les paiement en liquide ou par virement. Pour d'autres options, merci de nous contacter.
Les frais d'envoi sont à votre charge et le livre est à _prix libre_. Merci de votre soutien !

